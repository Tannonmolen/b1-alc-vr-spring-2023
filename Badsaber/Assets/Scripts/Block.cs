using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BlockColor
{
    Green,
    Red

}



public class Block : MonoBehaviour
{
    public BlockColor color;

    public GameObject brokenBlockLeft;
    public GameObject brokenBlockRight;
    public float brokenBlockForce;
    public float brokenBlockTorque;
    public float brokenBlockDestroyDelay;

    void OnTriggerEnter (Collider other)
    {
        if(other.CompareTag("SwordRed"))
        {
            if (color == BlockColor.Red)
            {
                GameManager.instance.AddScore();

            }
            else
            {
                GameManager.instance.HitWrongBlock();
                Hit();
            }
        }
        else if(other.CompareTag("SwordGreen"))
        {
            if (color == BlockColor.Green)
            {
                GameManager.instance.AddScore();
            }
            else 
            {
                GameManager.instance.HitWrongBlock();
            }
            Hit();
        }

    }

    void Hit ()
    {
        // Enable the broken pieces
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);

        // Remove them as children
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;

        // add force
        Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();

        leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);

        // add torqe to them
        leftRig.AddTorque(-transform.forward * brokenBlockTorque, ForceMode.Impulse);
        rightRig.AddTorque(transform.forward * brokenBlockTorque, ForceMode.Impulse);

        //Destory the borken pieces after a few seconds
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);

        // destroy the main block
        Destroy(gameObject);


    }


}
